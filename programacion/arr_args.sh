
#!/bin/bash


# se guardan todos los argumentos pasados al script como
# elementos de un array
args=( $@ )


# si la cantidad de argumentos es 1
# rellenamos el resto de indices con un valor
# para utilizarlo en futuras evualuaciones
# para que no de error

#if [ ${#args[@]} -eq 1 ]; then

#        args[1]=null

#fi

# alternativamente si la variable o indice de un array
# no tiene ningun valor podemos asignar uno en funcion de dicha ausencia
# en el llamado a la variable o indice de array
# en varaibles
# ${var:-valor}
# en arrays
# ${arr[2]:-valor}


if [ ${#args[@]} -lt 1 ]; then
        echo "Modo de uso: $0 date year month day" 
        exit 1
fi



if [ ${args[0]} = date ] && [ ${args[1]:-null} = year ]; then

        date +%Y


elif [ ${args[0]} = date ] && [ ${args[1]:-null} = month ]; then


        date +%m

elif [ ${args[0]} = date ] && [ ${args[1]:-null} = day ];then

        date +%d
else
        date
fi
#EOF
