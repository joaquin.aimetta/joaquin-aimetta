
#!/bin/bash

# Variable incremental en con valor '0'

i=0


# La condicion devuelve 'true' siempre que '$i' sea menor o igual que '5'

while [ $i -le 5 ];do
        
        # se imprime el valor de '$i'
        echo $i

        # se suma '1' al valor de '$i'
        ((i++))

done







