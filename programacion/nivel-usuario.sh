#!/bin/bash

# Si quien ejecuta el script no es 'root'
# el programa debe interrupirse y mostrar una leyenda

if [ $UID -ne 0 ]; then

    echo "Ejecute '$0' como usuario 'root' o 'sudo $0'"

    exit


fi
