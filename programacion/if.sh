#!/bin/bash

# Evaluar la existencia de un usuario en el fichero 'passwd'

read -p "Usuario: " usuario

# La opcion '-w' de grep hace que se busque un patron conicidente de manera
# exacta al patro ingrasado, de lo contrario busca el patron dentro de cadnas
# de caracteres sin importar que caracteres hayan de forma contigua.
# el signo '^' adelante del patron indica que el patron debe estar 
# al inicio de la linea, y al inicio de cada lina figura el nombre de usuario

exitstatus=$(grep 'bash' /etc/passwd > /dev/null 2>&1 ; echo $?)

if [ $exitstatus -eq 0 ]; then

    info_usuario=$(grep -w ^$usuario /etc/passwd)
    #Aca se le asigna el valor de  /etc/passwd de $usuario a "info_usuario"
    id_usuario=$(echo $info_usuario | cut -d ":" -f 3)
    #luego se le sectoriza de toda la linea que equivale a "info_usuario" que es lo que debe imprimir en pantalla
    shell_usuario=$(echo $info_usuario | cut -d ":" -f 7)


    echo $usuario $id_usuario $shell_usuario


fi
#EOF

