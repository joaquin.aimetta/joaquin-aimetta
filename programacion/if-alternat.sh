#!/bin/bash
set -e

# Evaluar la existencia de un usuario en el fichero 'passwd'

read -p "Usuario: " usuario

# Declaramos la variable al tiempo que hacemos la evaluacion
# luego utlizamos la variable en el cuerpo 'then'
# Los corchetes se utilizan cuando se comparan dos elemetnos
# [ $nombre = $input ]

if info_usuario=$(grep -w ^$usuario /etc/passwd); then

        id_usuario=$(echo $info_usuario | cut -d ":" -f 3)
        shell_usuario=$(echo $info_usuario | cut -d ":" -f 7)
        echo $usuario $id_usuario $shell_usuario
        
else
        echo "no se encuentra el usuario"
        exit 1
fi
#EOF
