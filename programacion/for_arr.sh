#!/bin/bash


# imprimir la lista de libros pero cuando se llega a 'Don Quixote" indicar que esta agotado.

BOOKS=('In Search of Lost Time' 'Don Quixote' 'Ulysses' 'The Great Gatsby')

for book in "${BOOKS[@]}"; do

	if [ "$book" = "Don Quixote" ]; then
		echo "Book: $book esta agotado"
	else
		echo "Book: $book"
	fi

done


