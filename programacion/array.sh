#!/bin/bash

array=( a b c d e )

echo "indice 0: ${array[0]}"
echo "indice 1: ${array[1]}"
echo "indice 2: ${array[2]}"
echo "indice 3: ${array[3]}"
echo "indice 4: ${array[4]}"

echo "El array '\$array' contiene ${#array[@]} elementos"

echo "Se agrega un elemto al indice 9"
echo "array[9]=f
"
array[9]=f

echo "El array '\$array' contiene ${#array[@]} elementos"

echo "Los indice del array son: ${!array[@]}"

echo "Los elementos guardados en el array son: ${array[@]}"


