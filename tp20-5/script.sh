

args=( $@ )

if [ ${#args[@]} -le "1"  ]; then

        echo "Debe indicar al menos un argumento mas ademas del hostname"
        exit 1
fi



for opcion in "${!args[@]}"; do

	case ${args[$opcion]} in

		-C)# cantidad de pings Joaquin!

			cantidad=${args[$(($opcion+1))]}
			
			if [[ "$cantidad" -ge "0" ]]; then
				counter="-c $cantidad"
			else
				echo "Para indicar la cantidad debe utilizar un numero entero"
				exit 1
			fi
			;;

		-T)# opcion -D para timestamp
			timestamp="-D"
			;;

		-p)# protocolo 4 o 6
			proto="${args[$(($opcion+1))]}"
			
			if [[ $proto =~ [46] ]]; then
				p="-$proto"
			else
				echo "Para indicar el protocolo debe usar 4 o 6"
				exit 1
			fi
			;;

		-b)#
			b="-b"
			;;
	esac

done


if [ ${args[-1]} == $-b ];then

	echo "Debe haber un hostname como ultimo argumento"
	exit 1
fi

ping $b $timestamp $p $counter ${args[-1]}



